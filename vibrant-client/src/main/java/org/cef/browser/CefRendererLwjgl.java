package org.cef.browser;

import net.cydhra.vibrant.VibrantClient;
import org.lwjgl.opengl.EXTBgra;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;

/**
 * This is a custom reimplementation of {@link CefRenderer} that uses LWJGL instead of JOGL. This is required to stay interoperable with
 * Minecraft. The implementation is a modified version of Montoyo's CefRenderer from github (see the link below).
 *
 * It basically adapts the behaviour of the original {@link CefRenderer} but replaces calls to JOGL's OpenGL Bindings to respective LWJGL
 * calls.
 *
 * @see <a href="https://github.com/montoyo/mcef/blob/master/src/main/java/org/cef/browser/CefRenderer.java">Montoyo's CefRenderer</a>
 */
public class CefRendererLwjgl {
    private final boolean transparent;
    int[] textureIdArray = new int[1];
    private int viewWidth = 0;
    private int viewHeight = 0;
    private Rectangle popupRectangle = new Rectangle(0, 0, 0, 0);
    private Rectangle originalPopupRectangle = new Rectangle(0, 0, 0, 0);

    CefRendererLwjgl(boolean transparent) {
        this.transparent = transparent;
        initialize();
    }

    /**
     * Initialize the renderer by creating a texture applying required GL parameters to the texture that is later used in rendering.
     */
    protected void initialize() {
        VibrantClient.INSTANCE.getGlStateManager().enableTexture2D();
        textureIdArray[0] = glGenTextures();

        VibrantClient.INSTANCE.getGlStateManager().bindTexture(textureIdArray[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        VibrantClient.INSTANCE.getGlStateManager().bindTexture(0);
    }

    /**
     * Delete the allocated texture from video RAM.
     */
    protected void cleanup() {
        if (textureIdArray[0] != 0) { glDeleteTextures(textureIdArray[0]); }
    }

    /**
     * Binds the texture that has been used while drawing the browser content and draw it into the given rect onto the current framebuffer.
     *
     * @param x1 target rectangle first corner x coordinate
     * @param y1 target rectangle first corner y coordinate
     * @param x2 target rectangle second corner x coordinate
     * @param y2 target rectangle second corner y coordinate
     */
    public void render(double x1, double y1, double x2, double y2) {
        if (viewWidth == 0 || viewHeight == 0) { return; }

        // TODO utilize the tessellator for rendering the quad
        VibrantClient.INSTANCE.getGlStateManager().bindTexture(textureIdArray[0]);
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glTexCoord2d(0.0, 1.0);
        GL11.glVertex2d(x1, y1);
        GL11.glTexCoord2d(1.0, 1.0);
        GL11.glVertex2d(x2, y1);
        GL11.glTexCoord2d(1.0, 0.0);
        GL11.glVertex2d(x2, y2);
        GL11.glTexCoord2d(0.0, 0.0);
        GL11.glVertex2d(x1, y2);
        GL11.glEnd();
        VibrantClient.INSTANCE.getGlStateManager().bindTexture(0);
    }

    void onPopupSize(Rectangle rect) {
        if (rect.width <= 0 || rect.height <= 0) { return; }
        originalPopupRectangle = rect;
        popupRectangle = getPopupRectInWebView(originalPopupRectangle);
    }

    private Rectangle getPopupRectInWebView(Rectangle rc) {
        // if x or y are negative, move them to 0.
        if (rc.x < 0) { rc.x = 0; }
        if (rc.y < 0) { rc.y = 0; }
        // if popup goes outside the view, try to reposition origin
        if (rc.x + rc.width > viewWidth) { rc.x = viewWidth - rc.width; }
        if (rc.y + rc.height > viewHeight) { rc.y = viewHeight - rc.height; }
        // if x or y became negative, move them to 0 again.
        if (rc.x < 0) { rc.x = 0; }
        if (rc.y < 0) { rc.y = 0; }
        return rc;
    }

    void clearPopupRects() {
        popupRectangle.setBounds(0, 0, 0, 0);
        originalPopupRectangle.setBounds(0, 0, 0, 0);
    }

    /**
     * Draws the current browser contents onto the texture associated with this instance.
     */
    void onPaint(boolean popup, Rectangle[] dirtyRects, ByteBuffer buffer, int width, int height, boolean completeReRender) {
        if (transparent) // Enable alpha blending.
        { VibrantClient.INSTANCE.getGlStateManager().enableBlend(); }

        final int size = (width * height) << 2;
        if (size > buffer.limit()) {
            System.out.println("Bad data passed to CefRenderer.onPaint() triggered safe guards... (1)");
            return;
        }
        // Enable 2D textures.
        VibrantClient.INSTANCE.getGlStateManager().enableTexture2D();
        VibrantClient.INSTANCE.getGlStateManager().bindTexture(textureIdArray[0]);

        int oldAlignement = glGetInteger(GL_UNPACK_ALIGNMENT);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        if (!popup) {
            if (completeReRender || width != viewWidth || height != viewHeight) {
                // Update/resize the whole texture.
                viewWidth = width;
                viewHeight = height;
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, viewWidth, viewHeight, 0, EXTBgra.GL_BGRA_EXT, GL_UNSIGNED_BYTE, buffer);
            } else {
                glPixelStorei(GL_UNPACK_ROW_LENGTH, viewWidth);

                // Update just the dirty rectangles.
                for (Rectangle rect : dirtyRects) {
                    if (rect.x < 0 || rect.y < 0 || rect.x + rect.width > viewWidth || rect.y + rect.height > viewHeight) {
                        System.out.println("Bad data passed to CefRenderer.onPaint() triggered safe guards... (2)");
                    } else {
                        glPixelStorei(GL_UNPACK_SKIP_PIXELS, rect.x);
                        glPixelStorei(GL_UNPACK_SKIP_ROWS, rect.y);
                        glTexSubImage2D(GL_TEXTURE_2D,
                                        0,
                                        rect.x,
                                        rect.y,
                                        rect.width,
                                        rect.height,
                                        EXTBgra.GL_BGRA_EXT,
                                        GL_UNSIGNED_BYTE,
                                        buffer
                        );
                    }
                }

                glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
                glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
                glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
            }
        } else if (popupRectangle.width > 0 && popupRectangle.height > 0) {
            int skip_pixels = 0, x = popupRectangle.x;
            int skip_rows = 0, y = popupRectangle.y;
            int w = width;
            int h = height;

            // Adjust the popup to fit inside the view.
            if (x < 0) {
                skip_pixels = -x;
                x = 0;
            }
            if (y < 0) {
                skip_rows = -y;
                y = 0;
            }
            if (x + w > viewWidth) { w -= x + w - viewWidth; }
            if (y + h > viewHeight) { h -= y + h - viewHeight; }

            // Update the popup rectangle.
            glPixelStorei(GL_UNPACK_ROW_LENGTH, width);
            glPixelStorei(GL_UNPACK_SKIP_PIXELS, skip_pixels);
            glPixelStorei(GL_UNPACK_SKIP_ROWS, skip_rows);
            glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, EXTBgra.GL_BGRA_EXT, GL_UNSIGNED_BYTE, buffer);
            glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
            glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
            glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);
        }

        glPixelStorei(GL_UNPACK_ALIGNMENT, oldAlignement);
        VibrantClient.INSTANCE.getGlStateManager().bindTexture(0);
    }
}

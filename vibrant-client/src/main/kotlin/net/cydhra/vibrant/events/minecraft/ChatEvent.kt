package net.cydhra.vibrant.events.minecraft

import net.cydhra.eventsystem.events.Event

/**
 * Event that is triggered, whenever the player submits a chat message
 */
class ChatEvent(val message: String) : Event() {

}
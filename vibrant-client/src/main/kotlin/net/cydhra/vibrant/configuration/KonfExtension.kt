package net.cydhra.vibrant.configuration

import com.uchuhimo.konf.Item
import kotlin.reflect.KProperty

operator fun <T> Item<T>.getValue(thisRef: Any, property: KProperty<*>): T {
    return ConfigurationService.config[this]
}

operator fun <T> Item<T>.setValue(thisRef: Any, property: KProperty<*>, newValue: T) {
    ConfigurationService.config[this] = newValue
}
package net.cydhra.vibrant.configuration.mapper

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.node.TextNode
import java.awt.Color

/**
 * Serializes a [Color] object into a JSON object
 */
class ColorSerializer : JsonSerializer<Color>() {
    override fun serialize(value: Color, gen: JsonGenerator, serializers: SerializerProvider) {
        gen.writeStartObject()
        gen.writeFieldName("argb")
        gen.writeString(Integer.toHexString(value.rgb))
        gen.writeEndObject()
    }
}

/**
 * Deserializes a JSON object into a color object. Expects the JSON object structure like as following:
 * ```
 * {
 *   "argb": "0xAABBCCDD"
 * }
 * ```
 */
class ColorDeserializer : JsonDeserializer<Color>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Color {
        val rgbaNode = p.codec.readTree<TreeNode>(p).get("argb") as TextNode
        return Color(Integer.parseUnsignedInt(rgbaNode.textValue(), 16), true)
    }
}
package net.cydhra.vibrant.api.world

interface VibrantBlockInfo {

    val lightOpacityValue: Int
    val renderingType: VibrantBlockRenderType

    fun normalCube(): Boolean

    fun translucent(): Boolean
}

enum class VibrantBlockRenderType(val renderId: Int) {

    UNKNOWN(0),
    LIQUID(1),

    /** tile entity special rendering */
    TESR(2),
    NORMAL_CUBE(3),

    NO_RENDER(-1);

    companion object {
        @JvmStatic
        fun byRenderId(renderType: Int): VibrantBlockRenderType? {
            return values().find { it.renderId == renderType }
        }
    }
}
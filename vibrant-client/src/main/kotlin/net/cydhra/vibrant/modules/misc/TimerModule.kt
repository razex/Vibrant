package net.cydhra.vibrant.modules.misc

import net.cydhra.vibrant.configuration.ConfigurationService
import net.cydhra.vibrant.configuration.ModuleConfigurationSpec
import net.cydhra.vibrant.configuration.getValue
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

class TimerModule : Module(name = "Timer", initialKeycode = Keyboard.KEY_U) {

    companion object : ModuleConfigurationSpec("Timer") {
        val speed by optional(2f)
    }

    val speed by Companion.speed

    init {
        ConfigurationService.addSpecification(TimerModule)
    }

    override fun onEnable() {
        mc.timer.timerSpeed = speed
    }

    override fun onDisable() {
        mc.timer.timerSpeed = 1f
    }

}

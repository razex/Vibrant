package net.cydhra.vibrant.modules.combat

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.entity.VibrantEntity
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.util.enemy.EnemyTracker
import net.cydhra.vibrant.util.enemy.ITrackedEntity
import net.cydhra.vibrant.util.math.Angle
import org.lwjgl.input.Keyboard

class KillauraModule : Module("Killaura", DefaultCategories.COMBAT, Keyboard.KEY_R) {

    private var lastAngle = Angle(0.0, 0.0)
    private var target: ITrackedEntity? = null

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer == null)
            return

        target = EnemyTracker.getClosestEntity(mc.thePlayer!!, VibrantEntity::class.java)

        if (target != null
                && target!!.entity is VibrantEntity && (target!!.entity as VibrantEntity)
                        .getDistanceSquared(mc.thePlayer!!.posX, mc.thePlayer!!.posY, mc.thePlayer!!.posZ) <= (3.8 * 3.8)) {

            val angle = Angle(target!!.entity)

            val yawDiff = angle.yaw - lastAngle.yaw
            val pitchDiff = angle.pitch - lastAngle.pitch

            lastAngle = angle

            mc.thePlayer!!.setEntityRotation((angle.yaw + yawDiff).toFloat(), (angle.pitch + pitchDiff).toFloat())

            mc.thePlayer!!.swing()
            mc.playerController!!.attackEntity(target!!.entity as VibrantEntity)
        }
    }
}
package net.cydhra.vibrant.gui.util.fontrenderer;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for maintaining fonts
 *
 * @author Flaflo
 */
public enum Fonts {

    INSTANCE;

    private Map<Integer, Font> cache;

    private Fonts() {
        this.cache = new HashMap<>();
    }

    /**
     * @param name the font name
     * @param style the font style
     * @param size the font size
     *
     * @return the font
     */
    public Font get(String name, int style, int size) {
        int hash = name.hashCode() ^ style ^ size;
        Font font = this.cache.get(hash);

        if (font == null) {
            font = new Font(name, style, size);
            this.cache.put(hash, font);
        }

        return font;
    }

}

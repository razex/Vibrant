package net.cydhra.vibrant.command

enum class CommandSender {
    PLAYER, BROWSER, CLIENT
}
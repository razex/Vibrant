package net.cydhra.vibrant.mixin.entity;

import net.cydhra.vibrant.api.client.VibrantMovementInput;
import net.cydhra.vibrant.api.entity.VibrantPlayerSP;
import net.cydhra.vibrant.api.inventory.VibrantContainer;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.cydhra.vibrant.api.util.chat.VibrantChatComponent;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.player.PlayerCapabilities;
import net.minecraft.inventory.Container;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MovementInput;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityPlayerSP.class)
public abstract class EntityPlayerSPMixin implements VibrantPlayerSP {

    @Shadow
    public MovementInput movementInput;

    @Shadow
    public abstract void swingItem();

    @Shadow
    public abstract void addChatMessage(IChatComponent message);

    @Shadow
    public abstract void sendChatMessage(String message);

    @Override
    public void swing() {
        this.swingItem();
    }

    @NotNull
    @Override
    public VibrantMovementInput getMovementInput() {
        return (VibrantMovementInput) this.movementInput;
    }

    @Override
    public void displayChatMessageOnClient(@NotNull VibrantChatComponent message) {
        this.addChatMessage((IChatComponent) message);
    }

    @Override
    public void sendChatMessageToServer(@NotNull String message) {
        this.sendChatMessage(message);
    }
}

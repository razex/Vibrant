package net.cydhra.vibrant.mixin.world.storage;

import net.minecraft.world.storage.SaveFormatComparator;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(SaveFormatComparator.class)
public abstract class SaveFormatComparatorMixin {
}

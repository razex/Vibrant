package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantVertexBufferBuilder;
import net.cydhra.vibrant.api.render.VibrantVertexFormat;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(WorldRenderer.class)
public abstract class VertexBufferBuilderMixin implements VibrantVertexBufferBuilder {

    @Shadow
    public abstract void begin(int drawMode, VertexFormat vertexFormat);

    @Override
    public void beginDrawing(int drawMode, @NotNull VibrantVertexFormat vertexFormat) {
        this.begin(drawMode, (VertexFormat) vertexFormat);
    }
}

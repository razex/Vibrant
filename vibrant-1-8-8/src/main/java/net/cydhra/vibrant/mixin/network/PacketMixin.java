package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantPacket;
import net.minecraft.network.Packet;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(Packet.class)
public interface PacketMixin extends VibrantPacket {
}

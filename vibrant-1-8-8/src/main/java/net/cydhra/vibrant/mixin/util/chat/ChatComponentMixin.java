package net.cydhra.vibrant.mixin.util.chat;

import net.cydhra.vibrant.api.util.chat.VibrantChatComponent;
import net.cydhra.vibrant.api.util.chat.VibrantChatStyle;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.IChatComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Arrays;
import java.util.List;

@Mixin(IChatComponent.class)
public interface ChatComponentMixin extends VibrantChatComponent {

    @Shadow
    IChatComponent setChatStyle(ChatStyle chatStyle);

    @Shadow
    IChatComponent appendSibling(IChatComponent iChatComponent);

    @Shadow
    List<IChatComponent> getSiblings();

    @Override
    public default VibrantChatComponent setStyle(VibrantChatStyle style) {
        return (VibrantChatComponent) this.setChatStyle((ChatStyle) style);
    }

    @Override
    public default VibrantChatComponent appendSiblingComponent(VibrantChatComponent component) {
        return (VibrantChatComponent) this.appendSibling((IChatComponent) component);
    }

    @Override
    public default List<VibrantChatComponent> getComponentSiblings() {
        return Arrays.asList(this.getSiblings().toArray(new VibrantChatComponent[0]));
    }
}

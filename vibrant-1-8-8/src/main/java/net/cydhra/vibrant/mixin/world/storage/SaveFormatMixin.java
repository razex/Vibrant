package net.cydhra.vibrant.mixin.world.storage;

import net.cydhra.vibrant.api.world.storage.VibrantSaveFormat;
import net.cydhra.vibrant.api.world.storage.VibrantSaveFormatComparator;
import net.minecraft.client.AnvilConverterException;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.SaveFormatComparator;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.List;
import java.util.stream.Collectors;

@Mixin(ISaveFormat.class)
public interface SaveFormatMixin extends VibrantSaveFormat {

    @Shadow
    List<SaveFormatComparator> getSaveList() throws AnvilConverterException;

    @Shadow
    void renameWorld(String worldName, String newName);

    @Shadow
    boolean deleteWorldDirectory(String folder);

    @NotNull
    @Override
    default public List<VibrantSaveFormatComparator> getSaves() {
        try {
            return this.getSaveList().stream()
                    .map(element -> (VibrantSaveFormatComparator) element)
                    .collect(Collectors.toList());
        } catch (AnvilConverterException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    default void rename(@NotNull String worldName, @NotNull String newName) {
        this.renameWorld(worldName, newName);
    }

    @Override
    default boolean deleteDirectory(@NotNull String folder) {
        return this.deleteWorldDirectory(folder);
    }
}

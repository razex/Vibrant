package net.cydhra.vibrant.tweaker

import net.cydhra.vibrant.VibrantClient
import net.minecraft.launchwrapper.ITweaker
import net.minecraft.launchwrapper.LaunchClassLoader
import org.spongepowered.asm.launch.MixinBootstrap
import org.spongepowered.asm.mixin.MixinEnvironment
import org.spongepowered.asm.mixin.MixinEnvironment.Side.CLIENT
import org.spongepowered.asm.mixin.Mixins
import java.io.File
import java.util.*

private const val MINECRAFT_MAIN_CLASS = "net.minecraft.client.main.Main"
private const val MIXIN_CONFIG = "mixins.vibrant.json"

/**
 * [ITweaker] implementation of the Minecraft Launchwrapper API that enables the Mixin bootstrap thus invoking class merging.
 */
class LaunchTweaker : ITweaker {

    private lateinit var arguments: Array<String>

    override fun acceptOptions(args: MutableList<String>, gameDirectory: File?, assetsDirectory: File?, version: String?) {
        this.arguments = mutableListOf<String>()
                .apply { addAll(args) }
                .also {
                    if (gameDirectory != null) {
                        it += "--gameDir"
                        it += gameDirectory.absolutePath
                    }

                    if (assetsDirectory != null) {
                        it += "--assetsDir"
                        it += assetsDirectory.absolutePath
                    }

                    if (version != null) {
                        it += "--version"
                        it += version
                    }
                }
                .toTypedArray()
    }

    override fun getLaunchTarget(): String {
        return MINECRAFT_MAIN_CLASS
    }

    override fun injectIntoClassLoader(classLoader: LaunchClassLoader) {
        MixinBootstrap.init()
        Mixins.addConfiguration(MIXIN_CONFIG)
        MixinEnvironment.getDefaultEnvironment().side = CLIENT
    }

    override fun getLaunchArguments(): Array<String> {
        return arguments
    }

}